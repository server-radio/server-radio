const ytdl = require('ytdl-core');


const database = require('../db/channeldb');
const ytdl_wrapper = require('../ytdl-wrapper');
const messageHandler = require('../msg/msg_handler')

module.exports = {
	name: 'add',
	aliases: [/ad{1,2}/, /\+/, /put/, /ad{1,2}-*song/],
	description: 'Adds a song to the channel',
	usage: 'radio add <channel_name> <video_url/playlist_url>',
	async execute(message, args) {
		messageHandler.validateArgs(args,4)

		let serverId = message.guild.id;
		let channelName = args[2];
		let songUrl = args[3];

		let songIdList = await ytdl_wrapper.getVideoIDList(songUrl);
		await database.addSongList(serverId, channelName, songIdList);
		
		
	}

}