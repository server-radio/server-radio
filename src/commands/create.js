const database = require('../db/channeldb');
const messageHandler = require('../msg/msg_handler')


module.exports = {
	name: 'create',
	aliases: [/cre+ate?-*([(channel)(station)(playlist)])?/],
	description: 'Creates a new channel',
	usage: 'radio create <channel_name>',
	async execute(message, args) {
		messageHandler.validateArgs(args,3)
		
		let serverId = message.guild.id;
		let channel = await database.createChannel(serverId,args[2]);
		await messageHandler.respondSuccess(message,'create_channel_success',[args[2]])
		
	}
}