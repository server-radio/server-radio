const database = require('../db/channeldb');
const messageHandler = require('../msg/msg_handler')


module.exports = {
	name: 'deleteChannel',
	aliases: [/delete-*channel/],
	description: 'Deletes a channel',
	usage: 'radio deleteChannel <channel_name>',
	async execute(message, args) {
		messageHandler.validateArgs(args,3)

		let serverId = message.guild.id;
		let channelName = args[2]

		await database.deleteChannel(serverId,channelName)
		await messageHandler.respondSuccess(message,'delete_channel_success',[channelName])
		
	}
}