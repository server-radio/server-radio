const database = require('../db/channeldb');
const messageHandler = require('../msg/msg_handler')


module.exports = {
	name: 'deleteSong',
	aliases: [/delete-*song/],
	description: 'Deletes a song from a channel',
	usage: 'radio deleteSong <channel_name> <song_name>',
	async execute(message, args) {
		messageHandler.validateArgs(args,4)
		
		let serverId = message.guild.id;
		let song = args[3]
		
		let channel = await database.getChannelData(serverId, args[2])
		

		channel.removeSong(song)
		await messageHandler.respondSuccess(message, `delete_song_success`,[song])
		
	}

}
