const msgHandler = require('../msg/msg_handler')

module.exports = {
	name: 'help',
	aliases: [/help/, /commands?/],
	description: 'Displays a list of commands',
	usage: 'radio help [command]',
	async execute(message, args) {
		await msgHandler.respondHelp(message)
	}
}