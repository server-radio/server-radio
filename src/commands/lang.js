const langdb = require('../db/langdb')
const messageHandler = require('../msg/msg_handler')


module.exports = {
	name: 'lang',
	aliases: [/lang/],
	description: 'Renames a channel',
	usage: 'radio rename <channel_name> <new_channel_name>',
	async execute(message,args){
		

		let serverId = message.guild.id;
		let lang = await langdb.getLang(serverId)
		let langid = lang.id
		console.log(lang)
		console.log('running...')
		if(args.length==2){
			messageHandler.respondSuccess(message,'lang_show',[langid])
		}
		else{

			let newLang = args[2]
			langdb.changeLang(serverId,newLang)
			messageHandler.respondSuccess(message,'lang_set',[langid,newLang])
		}
	}
}