const database = require('../db/channeldb');
const messageHandler = require('../msg/msg_handler')


module.exports = {
	name: 'list',
	aliases: [/list.*/],
	description: 'Lists all playlists',
	usage: 'radio list',
	async execute(message, args) {
		let serverId = message.guild.id;
		let value = await database.getChannelList(serverId);

		if (value.length == 0) {
			throw "ERR_NO_PLAYLISTS"
		}

		let cont = '```';
		value.forEach((key, index) => {
			cont += `${index + 1}.\t${key}\t\n`;
		});
		cont += '```'
		message.channel.send(cont);

	}
}
