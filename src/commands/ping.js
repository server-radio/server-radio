module.exports = {
	name: 'ping',
	aliases: [/ping/],
	description: 'Pings the bot',
	usage: 'radio ping',
	execute(message, args) {
		message.channel.send('Pong');
	}
}