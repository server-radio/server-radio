const ytdl = require('ytdl-core');

const database = require('../db/channeldb');
const messageHandler = require('../msg/msg_handler')
const voice = require('../voice/voice')

module.exports = {
	name: 'play',
	aliases: [/play.*/],
	description: 'Plays the channel',
	usage: 'radio play <channel_name>',
	async execute(message, args) {
		messageHandler.validateArgs(args,3)

		let serverId = message.guild.id;
		let channelName = args[2];
		let value = await database.getChannelData(serverId, channelName);
		let channelArray = value.songs;

		await voice.play(message,channelArray)

		await messageHandler.respondSuccess(message,'playing_channel',[channelName])
	}

}
