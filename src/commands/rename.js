const database = require('../db/channeldb');
const messageHandler = require('../msg/msg_handler')

module.exports = {
	name: 'rename',
	aliases: [/rename/, /rename-*channel/],
	description: 'Renames a channel',
	usage: 'radio rename <channel_name> <new_channel_name>',
	async execute(message, args) {
		messageHandler.validateArgs(args,4)
		
		
		let serverId = message.guild.id;
		let channelName = args[2];
		let newChannelName = args[3];

		let value = await database.getChannelData(serverId, channelName)

		value.rename(newChannelName)
		await messageHandler.respondSuccess(message, 'rename_channel_success',[channelName,newChannelName]);

	}
}