const ytld = require('ytdl-core');
const Discord = require('discord.js')
const MemCache = require('memory-cache');
var showCache = new MemCache.Cache();

const database = require('../db/channeldb');
const botmessage = require('../msg/botmessage');

const numperpage = 10

const getListEmbed = async function (message, channelData) {
	num = numperpage * showCache.get(message.id);
	slicedChannel = channelData.songs.slice(num, Math.min(channelData.songs.length, num + numperpage))
	txt = ''
	console.log(channelData.songs)
	console.log(slicedChannel)
	for (s in slicedChannel) {
		let song = slicedChannel[s]
		let truenum = +s + +num
		console.log(truenum)
		console.log(song)
		obj = await ytld.getBasicInfo(song)
		title = obj.videoDetails.title
		if (title.length > 60) {
			title = title.substring(0, 60) + '...'
		}
		txt += `${truenum}\t:${title}\t${song}\n`
	}

	return botmessage.getSongListEmbed(txt);
}

const adjustIndex = function (messageId, channelData, numAdjust) {
	let num = showCache.get(messageId)
	if (!num) {
		num = 0
	}
	num = numAdjust(num)
	num = Math.max(num, 0)
	num = Math.min(num, channelData.songs.length / numperpage - 1)

	showCache.put(messageId, num)

}

module.exports = {
	name: 'show',
	aliases: [/show.*/],
	description: `Lists the channel's songs`,
	usage: 'radio show <channel_name>',
	async execute(message, args) {
		if (args.length < 3) {
			botmessage.respondArgs(message)
			return;
		}
		let serverId = message.guild.id;
		let chann = await database.getChannelData(serverId, args[2])

		if (!chann) {
			botmessage.respondError(message, 'Not a channel');
			return;
		}

		if (chann.length <= 0) {
			botmessage.respondError(message, 'Channel has no songs :(')
			return;
		}

		const emojis = ['⬅️', '➡️']


		message = await botmessage.respondSuccess(message, `React with ${emojis[0]} or ${emojis[1]} to traverse the list!`);



		await message.react(emojis[0])
		await message.react(emojis[1])


		const filter = (reaction, user) => emojis.includes(reaction.emoji.name)
		const collector = message.createReactionCollector(filter, { time: 60000 });
		collector.on('collect', async function onReaction(react, user) {
			console.log(react.emoji.name)
			switch (react.emoji.name) {
				case emojis[0]:
					adjustIndex(message.id, chann, (n) => (--n));
					embed = await getListEmbed(message, chann)
					message.edit(embed)
					break;

				case emojis[1]:
					adjustIndex(message.id, chann, (n) => (++n))
					embed = await getListEmbed(message, chann)
					message.edit(embed)
					break;
			}

			react.users.remove(user)
		});


	}
}