const botmessage = require('../msg/botmessage');
const voice = require('../voice/voice');

module.exports = {
	name: 'stop',
	aliases: [/stop/, /leave/],
	description: 'Stops the bot',
	usage: 'radio stop',
	async execute(message, args) {
		let serverId = message.guild.id;
		await voice.stop(serverId)
	}
}