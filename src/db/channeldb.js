const MemCache = require('memory-cache')
const listCache = new MemCache.Cache()
const channelCache = new MemCache.Cache();

const query = require('./query')
const Channel = require('./class/channel')

const cacheLifetime = 1 * 1000;

const getChannelList = async function(serverId){
	let cacheGet = channelCache.get(serverId)
	if(cacheGet){
		return cacheGet;
	}
	
	let channels = await query.getChannels(serverId);
	let channelList = channels.map( value => value.name)


	listCache.put(serverId,channelList,cacheLifetime);

	return channelList;
}

const getChannelData = async function(serverId,channelName){
	let argjs = {serverId,channelName}
	let cacheGet = channelCache.get(argjs)
	if(cacheGet){
		return cacheGet
	}

	let res = await query.getChannelData(serverId,channelName)
	
	if(res.length!=1){
		if(res.length<1){
			throw new ReferenceError("ERR_CHANNEL_DOES_NOT_EXIST")
		}
		else{
			throw new Error("ERR_MULTIPLE_INSTANCE")
		}
	}

	let songs = res[0].songs;
	let channel = new Channel(serverId,channelName,songs);
	
	channelCache.put(argjs,channel,cacheLifetime,onChannelFree)


	return channel;
}

const onChannelFree = async function(argjs,channel){
	if(channel.isDeleted){
		return;
	}
	
	//TODO Make only occur in a single query
	if(channel.hasChanged){
		query.editChannelSongs(channel.serverId,channel.name,channel.songs)
	}
	
	if(channel.rename){
		query.renameChannel(channel.serverId,channel.name,channel.newName)
	}
}

const createChannel = async function(serverId,channelName){
	let channels = await getChannelList(serverId)
	
	console.log(channels)
	if(channels.includes(channelName)){
		throw new Error("ERR_CHANNEL_EXISTS")
	}

	channels.push(channelName)
	query.createChannel(serverId,channelName,[])

	let ch = new Channel(serverId,channelName,[])
	channelCache.put({serverId,channelName},ch,cacheLifetime,onChannelFree)
}

const deleteChannel = async function(serverId,channelName){
	let channels = await getChannelList(serverId)

	if(!channels.includes(channelName)){
		throw new Error("ERR_CHANNEL_NONEXISTENT")
	}


	//Removes it from cache
	let chlist = await getChannelList(serverId)
	if(chlist.includes(channelName)){
		chlist.splice(chlist.indexOf(channelName),1)
	}

	let ch = channelCache.get({serverId,channelName})
	if(ch){
		ch.delete()
	}

	await query.deleteChannel(serverId,channelName)
	
	
	return true
}

const addSongList = async function(serverId,channelName,songs){
	const channel = await getChannelData(serverId,channelName)
	channel.addSongList(songs)
}

module.exports = {
	getChannelList,
	getChannelData,
	createChannel,
	deleteChannel,
	addSongList
}