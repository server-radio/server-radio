module.exports = class{
	constructor(serverId,name,songs){
		this.serverId = serverId;
		this.name = name;
		this.songs = songs || new Array();
		this.isRenamed = false;
		this.newName = name;
		this.hasChanged = false;
		//TODO We probably don't want this
		this.isDeleted = false;
	}
	

	addSong(songId){
		if(!this.hasSong(songId)){
			this.songs.push(songId);
			this.hasChanged = true
		}
		else{
			throw new Error("ERR_SONG_EXISTS");
		}
	}

	addSongList(songs){
		songs.forEach(element => {
			if(!this.hasSong(element)){
				this.songs.push(element)
				this.hasChanged = true
			}
		});
	}

	removeSong(song){
		console.log(this.songs)
		let index = this.songs.indexOf(song)
		if(index<0){
			throw new Error("ERR_SONG_NONEXISTENT")
		}
		this.songs.splice(index,1)
		this.hasChanged = true
		return true
	}

	rename(newName){
		if(newName==this.name){
			throw new Error("ERR_SAME_NAME")
		}
		this.isRenamed = true
		this.newName = newName
	}

	hasSong(songId){
		return this.songs.includes(songId)
	}

	delete(){
		this.isDeleted = true
	}
}