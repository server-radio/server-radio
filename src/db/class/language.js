module.exports = class{
	constructor(id){
		this.id = id
		this.hasChanged = false
	}

	changeLang(newid){
		if(this.id==newid){
			throw new Error("ERR_SAME_LANG_ID")
		}
		this.hasChanged = true
		this.id = newid
	}
}