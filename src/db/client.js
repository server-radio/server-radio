const {Client}  = require('pg');

const envConfig = require('../env_config')


const client = new Client({
	user: envConfig.pguser,
    host: envConfig.pghost,
    database: envConfig.pgdb,
    password: envConfig.pgpassword,
    port: envConfig.pgport,
})


//TODO FIX THIS
client.connect()

module.exports = client;