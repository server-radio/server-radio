const MemCache = require('memory-cache')
const langCache = new MemCache.Cache()

const query = require('./query')
const Language = require('./class/language')

const cacheLifetime = 1*1000
const defaultLang = 'en'

const initLang = async function (serverId,lang){
	query.initLang(serverId,lang||defaultLang)
}

const getLang = async function(serverId){
	let cacheres = langCache.get(serverId)
	if(cacheres){
		return cacheres
	}

	let queres = await query.getLang(serverId)
	if(queres.length==1){
		let olang = new Language(queres[0].lang)
		langCache.put(serverId,olang,cacheLifetime,onLangFree)
		return olang
	}

	if(queres.length==0){
		//Inits lang
		let newLang = new Language(defaultLang)
		await initLang(serverId,newLang.id)
		langCache.put(serverId,newLang,cacheLifetime,onLangFree)
		return newLang
	}

	throw "ERR_MULTIPLE_LANG"
}

const changeLang = async function (serverId, newLang){
	let lang = await getLang(serverId)
	lang.changeLang(newLang)
}

const onLangFree = async function(serverId,lang){
	if(lang.hasChanged){
		query.editLang(serverId,lang.id)
	}
}

module.exports = {
	initLang,
	getLang,
	changeLang
}