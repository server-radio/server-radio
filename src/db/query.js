const client = require('./client')


//Note: this is a very weak wrapping: there are no checks beyond those provided by the psql library. These are not to be used directly.
module.exports = {
	

	//Channel Queries
	//TODO make an object of it with lengths
	async getChannels(serverId){
		let res = await client.query('select name from stations where server_id = $1::text',[serverId]);
		return res.rows;
	},

	async getChannelData(serverId,channelName){
		let res = await client.query('select songs from stations where server_id = $1::text and name = $2::text',[serverId,channelName])
		return res.rows;
	},

	async createChannel(serverId,channelName,songs){
		await client.query('insert into stations values ($1::text,$2::text,$3::text[])',[serverId,channelName,songs])
	},

	async editChannelSongs(serverId,channelName,newsongs){
		await client.query('update stations set songs = $3::text[] where server_id = $1::text and name = $2::text',[serverId,channelName,newsongs])
	},

	async renameChannel(serverId,channelName,newName){
		await client.query('update stations set name = $3::text where server_id = $1::text and name = $2::text',[serverId,channelName,newName])
	},

	async deleteChannel(serverId,channelName){
		await client.query('delete from stations where server_id = $1::text and name = $2::text',[serverId,channelName])
	},

	//Lang Queries
	async getLang(serverId){
		let res = await client.query('select lang from langs where server_id = $1::text',[serverId])
		return res.rows
	},

	async editLang(serverId,lang){
		await client.query('update langs set lang = $2 where server_id = $1::text',[serverId,lang])
	},

	async initLang(serverId,lang){
		await client.query('insert into langs values ($1::text,$2::text)',[serverId,lang])
	}

}