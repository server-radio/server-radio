require('dotenv').config()

exports.token = process.env.BOT_TOKEN

exports.pguser = process.env.PG_USER || 'postgres'
exports.pgdb = process.env.PG_DB || this.pguser
exports.pghost = process.env.PG_HOST || 'localhost'
exports.pgpassword = process.env.PG_PASSWORD
exports.pgport = process.env.PG_PORT || 5432