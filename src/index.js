//written by Samuel B Segal, Bijan Tahmassebi, and Veronica Starchenko
const Discord = require('discord.js');
const fs = require('fs');

const errorHandler = require('./msg/error_handler')
const { token } = require('./env_config.js')


const prefix = 'radio';
const client = new Discord.Client();
let commands = new Array(0);


const commandFiles = fs.readdirSync('./src/commands').filter(file => file.endsWith('.js'));


for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	commands.push(command);
	console.log(`Loaded command: ${command.name}`)
}


client.on('message', async function onMessage(message) {
	if (message.author.bot || !message.content.toLowerCase().startsWith(prefix)) {
		return;
	}

	var args = message.content.split(/\s+/);

	if (args.length < 2) {
		message.reply(`Not enough arguments! For help, put in "radio help"`);
		return;
	}

	let msgCom = args[1].toLowerCase();

	for (com of commands) {
		let aliases = com.aliases;
		for (ali of aliases) {
			if (ali.test(msgCom)) {
				try {
					await com.execute(message, args);
					//This can be removed if necessary
					await message.react('✅');
				}
				catch (err) {
					errorHandler.handleError(err,message,args)
				}
			}
		}
	}

});

client.on('ready', function onReady() {
	console.log('Init')
})

client.login(token);