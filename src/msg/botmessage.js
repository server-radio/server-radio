const Discord = require('discord.js')

const getErrorEmbed = function(txt){
	let embed  = new Discord.MessageEmbed()
		.setColor('F04444')
		.setTitle('Error!')
		.setDescription(txt)
	return embed;
}

const getSuccessEmbed = function(txt){
	let embed = new Discord.MessageEmbed()
		.setColor('44F044')
		.setDescription(txt)
	return embed;
}

const getArgFailEmbed = function(){
	let embed = new Discord.MessageEmbed()
		.setColor('F04444')
		.setTitle('Error: Not enough args!')
	return embed
}

const getSongListEmbed = function(songListTest){
	let embed = new Discord.MessageEmbed()
		.setColor('44F044')
		.setTitle('songs')
	return embed
}

module.exports = {
	respondError(msg,txt){
		msg.channel.send( getErrorEmbed(txt) )
	},

	async respondSuccess(msg,txt){
		return await msg.channel.send( getSuccessEmbed(txt) )
	},

	respondArgs(msg){
		msg.channel.send( getArgFailEmbed() )
	},

	getSongListEmbed(songListTest){
		let embed = new Discord.MessageEmbed()
			.setColor('44F044')
			.setTitle('Songs')
			.setDescription(songListTest)
		return embed
	}
}