const botmessage = require('./botmessage')

const errorToMsg = {
	'ERR_SONG_EXISTS': 'Song already exists!',
	'ERR_SAME_NAME': 'Renamed channel has same name',
	'ERR_CHANNEL_EXISTS': 'Channel already exists',
	'ERR_CHANNEL_NONEXISTENT': 'Channel does not exist',
	'ERR_MULTIPLE_INSTANCE': 'There are multiple channels with that name. This should never show up.'
	
}

module.exports = {


	async handleError(err,msg,args){
		switch(err.message){
		case "ERR_SONG_EXISTS":
		case "ERR_SAME_NAME":
		case "ERR_CHANNEL_EXISTS":
		case "ERR_CHANNEL_NONEXISTENT":
		case "ERR_MULTIPLE_INSTANCE":
			botmessage.respondError(msg,errorToMsg[err.message])
			break;
		case "ERR_ARG_LENGTH":
			botmessage.respondArgs(msg);
			break;
		default:
			botmessage.respondError(msg,err.message)
			console.log(err)
			
		}

	}
}