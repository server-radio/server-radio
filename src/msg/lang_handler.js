const fs = require('fs')

const langFiles = fs.readdirSync('./data/lang/').filter(file => file.endsWith('.json'))

let langJsons = {}

for(const file of langFiles){
	const lang = require(`../../data/lang/${file}`)

	let langName = lang.code
	langJsons[langName]=lang

}


module.exports.translateKey = function(langCode,key,args){
	
	if(!args){
		return langJsons[langCode][key]
	}

	let out = langJsons[langCode][key]
	for(i in args){
		let res = args[i]
		out = out.replace(`$${+i+1}`,args[i])
	}
	return out;
}