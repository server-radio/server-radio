const botmessage = require('./botmessage')
const langHandler = require('./lang_handler')
const langdb = require('../db/langdb')


exports.respondSuccess = async function(msg,key,args){
	let lang = await langdb.getLang(msg.guild.id)
	let content = langHandler.translateKey(lang.id,key,args)
	botmessage.respondSuccess(msg,content)
}

//Maybe this isn't the best place, but for now
exports.validateArgs = function(args,minArgs){
	if(args.length<minArgs){
		throw new Error("ERR_ARG_LENGTH")
	}
}

exports.respondHelp = async function(msg){
	let lang = await langdb.getLang(msg.guild.id)
	let arr = langHandler.translateKey(lang.id,'help')
	let str = arr.join('\n')
	await botmessage.respondSuccess(msg,str)
}