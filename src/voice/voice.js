const MemCache = require('memory-cache');
const voiceCache = new MemCache.Cache();

const VoiceConnect = require('./voice_connect')

module.exports = {
	async play(message,channelData){

		let serverId = message.guild.id

		if(voiceCache.get(serverId)){
			throw "ERR_ALREADY_PLAYING"
		}

		let connection = await message.member.voice.channel.join()
		console.log(connection)
		let voice = new VoiceConnect(connection,channelData)
		voiceCache.put(serverId,voice)
		voice.play()
	},

	async stop(serverId){
		let cacheres = voiceCache.get(serverId)
		if(!cacheres){
			throw new Error("ERR_NO_RADIO_CONN")
		}
		cacheres.stop()
		voiceCache.del(serverId)
	}
}