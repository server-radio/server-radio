const ytdl = require('ytdl-core')

const maxBagSize = 10

module.exports = class{
	constructor(voiceconnection,songs){
		this.voiceconnection = voiceconnection
		this.songs = songs
		this.currbag = new Array()
		this.backlog = new Array()
		this.bagSize = Math.min(this.songs.length,maxBagSize)

		if(this.bagSize<=0){
			throw "ERR_NO_SONGS"
		}

		//Copies everything into backlog
		for(const s of this.songs){
			this.backlog.push(s)
		}

		while(this.currbag.length<this.bagSize){
			this.#moveSongToCurrBag()
		}
	}

	play(){
		let newSong = this.nextSong()
		let dispatcher = this.voiceconnection.play(ytdl(newSong, { filter: 'audioonly' }));
		dispatcher.on('finish',this.#onSongEnd)
	}

	#onSongEnd(){
		//Continues playing
		if(this.voiceconnection){
			this.play()
		}
	}

	stop(){
		this.voiceconnection.disconnect()
	}

	#moveSongToCurrBag(){
		let ind = Math.floor(Math.random()*this.backlog.length)
		let val = this.backlog[ind]
		this.currbag.push(val)
		this.backlog.splice(ind,1)	
	}

	nextSong(){
		let val = this.currbag.shift()
		this.#moveSongToCurrBag()
		this.backlog.push(val)
		return val
	}
}