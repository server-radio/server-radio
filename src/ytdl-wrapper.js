const ytdl = require('ytdl-core');
const ytpl = require('ytpl');

var isPlaylist = function(url){
	return /(https:\/\/)?(www\.)?youtube\.com\/playlist\?list=.+/.test(url);
}

var getVideosFromPlaylist = async function(playlistUrl){	
	return await ytpl(await ytpl.getPlaylistID(playlistUrl));
} 

module.exports = {

	async getVideoIDList(url){
		let isPlay = isPlaylist(url);
		console.log(isPlay);
		if(isPlay){
			console.log('playlist');
			return (await getVideosFromPlaylist(url)).items.map( (vid)=>(vid.id) );
		}
		else{
			return [ytdl.getURLVideoID(url)];
		}
	}
}